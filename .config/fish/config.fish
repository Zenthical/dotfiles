set -U fish_greeting ""
#cat .cache/wal/sequences
clear && neofetch --ascii_distro archlabs 
fish_vi_key_bindings
alias vim "nvim"
alias update "sudo pacman -Syyu"
alias sync ".emacs.d/bin/doom sync"
alias icat "kitty +kitten icat"
alias ls "exa"
alias cat "bat"
alias grep "rg"

starship init fish | source
